# SortiesMEL

This is a simple project I wrote to practice with Angular and Spring. The app gets information about events in Lille and around from the api opendata.lillemetropole.fr
In these version, I added a securized access to the app with registering and logging features.

# License

MIT

# Get ready
Create a mysql database named "mel"
Run the project as a Spring Boot App to launch the server.
Use the MEL V1 Angular project for the front.